import socket
import player
import game
import threading

# -- welcomePlayer() : adds player to playerList
# -- matchMaker() -> inGameServer
# -- inGameServer(player1, player2) - thread per pair

"""
playerManager is responsible to accept incoming players and 
match existing player with each other concurrently.

self.__managerSocket - the game server's socket
self.__questions - the given set of questions
self.__playerSocketList - list of (object) Players   

"""
class playersManager:
    def __init__(self, ip, port, questions):
        self.__managerSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__port = port
        self.__ip = ip

        self.__questions = questions
        self.__playerSocketList = []

    """
    initialize the game socket and starting to look for player to match together
    """
    def initPlayerManager(self):
        self.__managerSocket.bind((self.__ip, self.__port))
        match = threading.Thread(target=self.matchMaker)
        match.start()
        self.waiteForPlayers()

    """
    scanning through __playerSocketList for Players to match together
    matching two Players in QUEUING state
    # staring a new Game instance and thread for the two players
    
    """
    def matchMaker(self):
        while True:
            playersPair = []
            for iPlayer in self.__playerSocketList:
                playerStatus = iPlayer.getStatus()
                if playerStatus == player.QUEUING:
                    playersPair.append(iPlayer)
                    if len(playersPair) == 2: # found two players
                        for p in playersPair: # setting status
                            p.setStatus(player.IN_GAME)
                        newGame = game.Game(playersPair, self.__questions)
                        print('Two Players have been Paired! ')
                        # staring a new Game instance and thread for the two players
                        threadGame = threading.Thread(target=newGame.initGame)
                        threadGame.start()
                elif playerStatus == player.FINISHED_GAME:
                    self.__playerSocketList.remove(iPlayer)

    """
    Welcoming incoming players and creating a new Player object for each
    adding the Player object to the __playerSocketList
    """
    def waiteForPlayers(self):
        print('Game Server is up! - Waiting for players...')
        self.__managerSocket.listen()
        while True:
            playerSck, addr = self.__managerSocket.accept()
            print('Player has connected')
            newPlayer = player.PlayerSocket(playerSck, addr,player.QUEUING)
            self.__playerSocketList.append(newPlayer)

