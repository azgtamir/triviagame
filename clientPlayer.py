import socket
import threading
#import visualsTrivia

IP = '127.0.0.1'
PORT = 5261

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def waitForMsg():
    while True:
        msg = sock.recv(1024)
        print(str(msg.decode()))


def main():
    print('Client is up!')
    sock.connect((IP,PORT))

    serverThread = threading.Thread(target=waitForMsg)
    serverThread.start()

    while True:
        userInput = input(">>")
        sock.send(userInput.encode())

#    while True:
#        screen = visualsTrivia.visualsTrivia(800,600)
#        screen.showOnScreen("Biggest Animal on Earth?" , True)



if __name__ == '__main__':
    main()

