import select

ALL = -1


"""
class Game - representing an ongoing game 
__setOfQuestions - given set of question
__players - given players 

"""

class Game:

    def __init__(self, players, setOfQuestions):
        self.__setOfQuestions = setOfQuestions
        self.__turn = 0
        self.__round = 2
        self.__players = players

    def nextTurn(self):
        if self.__turn+1 == len(self.__players):
            self.__turn = 0
        else:
            self.__turn += 1

    """
    sending messages according to addr attribute
    ALL - sending message to all of the Players
    (IP,PORT) - sending message to a specific Player  
    """
    def send(self, msg, addr):
        msg = ">>> " + msg + "\n"
        if addr == ALL:
            for player in self.__players:
                print('sending: ' + msg)
                player.getSocket().send(msg.encode())
        else:
            for player in self.__players:
                if player.getPlayerAddr() == addr:
                    print('sending' + msg)
                    player.send(msg.encode())


    """
    waiting for the right Player to answer and collecting it's answer 
    """
    def waitForAnswer(self, turn, thisRound):

        playerHasNotAnswered = True

        playersSocketList = [] # generating a list of sockets for SELECT
        for player in self.__players:
            playersSocketList.append(player.getSocket())

        while playerHasNotAnswered:
            readSocket, writeSocket, errSocket = select.select(playersSocketList,[] ,[])

            for sock in readSocket:
                if sock.getpeername() == self.__players[turn].getPlayerAddr(): # current player has answered
                    answer = str((sock.recv(1024)).decode())
                    if answer == list(self.__setOfQuestions[thisRound].values())[turn]:
                        playerHasNotAnswered = False
                        msg = 'Correct!'
                        self.__players[turn].upScore()
                        self.send(msg, self.__players[turn].getPlayerAddr())
                    else:
                        playerHasNotAnswered = False
                        msg = 'Wrong!'
                        self.send(msg, self.__players[turn].getPlayerAddr())
                else:
                    sock.recv(1024) # flushing socket
                    msg = 'Wait For Your Turn!'
                    self.send(msg, sock.getpeername())

    """
    sending the current question to everyone and waiting for the right player to answer
    """
    def askQuestion(self, turn, thisRound):
        for i, player in enumerate(self.__players):
            if i == self.__turn:
                msg = 'It\'s Your Turn!'
                self.send(msg, player.getPlayerAddr())
            else:
                msg = f'It\'s Player: {player.getPlayerAddr()} Turn!'
                self.send(msg, player.getPlayerAddr())

        currentQuestion = "Question is: " + list(self.__setOfQuestions[thisRound].keys())[turn]
        self.send(currentQuestion, ALL)
        self.waitForAnswer(turn,thisRound)


    """
    determine the winner and sending messages accordingly
    """
    def Winner(self):
        winnerIs = self.__players[0].getPlayerAddr()
        bestScore = self.__players[0].getScore()

        for player in self.__players:
            if player.getScore() > bestScore:
                bestScore = player.getScore()
                winnerIs = player.getPlayerAddr()

        for player in self.__players:
            if player.getPlayerAddr() == winnerIs:
                finalMsg = " --- YOU ARE THE WINNER --> With the Score of:" + str(bestScore) + '\n\n'
                finalMsg += "!!! Thanks For Playing !!!"
                self.send(finalMsg, winnerIs)
            else:
                finalMsg = "Winner is: " + str(winnerIs) + " --> With the Score of:" + str(bestScore) + '\n\n'
                finalMsg += "!!! Thanks For Playing !!!"
                self.send(finalMsg, player.getPlayerAddr())


    """
    iterate through the set of question and sending the current question 
    """
    def playGame(self):
        while self.__round != -1: # round
            for question in self.__setOfQuestions[self.__round]: #turn
                self.askQuestion(self.__turn, self.__round)
                self.nextTurn()
            self.__round -= 1
            if self.__round != -1:
                msg = f"<<  Next Round  >>"
                self.send(msg, ALL)

        self.Winner()

    """
    initialize the game 
    """
    def initGame(self):
        msg  = ( 'Welcome y\'all! ' +
                'We are going to play a little trivia game! Are you ready?' )
        self.send(msg,ALL)
        self.playGame()




