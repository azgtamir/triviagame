import pygame

pygame.init()

class visualsTrivia:
    def __init__(self, resX, resY):
        self.__running = False
        self.__window = pygame.display.set_mode((resX,resY))

    def showOnScreen(self, question, isTurn):
        self.__running = True

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.__running = False

        self.__window.fill((0, 0, 0))

        bigFont = pygame.font.SysFont("Comic Sans MS", 30)
        smallFont = pygame.font.SysFont("Comic Sans MS", 18)

        triviaString = bigFont.render("Trivia Time ", 1, (255, 00, 00))
        qString = smallFont.render("Question: " + question , 1,(255, 00, 00))

        self.__window.blit(triviaString, (300, 10))
        self.__window.blit(qString, (100, 100))

        if isTurn:
            aString = smallFont.render("Answer: ", 1, (255, 00, 00))
            self.__window.blit(aString, (100, 200))

        pygame.display.update()


