import playersManager
import questionDB

IP = '10.92.13.186'
PORT = 5262

"""
importing the questions dataBase in a form of Array of dictionaries as question:answer - allows modularity
injecting the questions dataBase and initialize the playerManager
"""

def main():
    questions = questionDB.questionDB()
    plyrManager = playersManager.playersManager(IP, PORT, questions.getQSet())
    plyrManager.initPlayerManager()

if __name__ == '__main__':
    main()
