
QUEUING = 0
IN_GAME = 1
FINISHED_GAME = 2

"""
PlayerSocket - representing a client (connection)
self.__status - representing the Player's status: QUEUING/IN_GAME/FINISHED_GAME

"""

class PlayerSocket:
    def __init__(self, playerSocket, playerAddr, status):
        self.__playerAddr = playerAddr
        self.__playerSocket = playerSocket
        self.__status = status
        self.__score = 0

    def getPlayerAddr(self):
        return self.__playerAddr

    def getSocket(self): #fix
        return self.__playerSocket

    def getStatus(self):
        return self.__status

    def send(self, msg):
        self.__playerSocket.send(msg)

    def setStatus(self, status):
        self.__status = status

    def getScore(self):
        return self.__score

    def upScore(self):
        self.__score += 1







