"""
questionDB - representing a question database: allows modularity
"""

class questionDB:
    def __init__(self):

        self.__questions1 = {'Name the highest mountain on earth?': 'Everest', 'Who was the first President of the US?': 'George Washington'}
        self.__questions2 = {'How tall is the Eiffel Tower?': '324m', 'The Big Ben is a nickname for?': 'The Greate Bell'}
        self.__questions3 = {'What is the biggest animal on earth?': 'Blue Whale', 'Name a synonym for an Orca?': 'Killer Whale'}

        self.__setOfQuestions = [self.__questions1, self.__questions2, self.__questions3]

    def getQSet(self):
        return self.__setOfQuestions